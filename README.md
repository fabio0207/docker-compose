# docker-compose

Este repo é uma amostra de serviços comuns sendo utilizados via docker-compose em ambiente de desenvolvimento.

**A imagem resultante deste Dockerfile deve ser utilizada somente em ambiente de desenvolvimento.**

> As palavras-chave "DEVE", "NÃO DEVE", "REQUER", "DEVERIA", "NÃO DEVERIA", "PODERIA", "NÃO PODERIA", "RECOMENDÁVEL", "PODE", e "OPCIONAL" neste documento devem ser interpretadas como descritas no [RFC 2119](http://tools.ietf.org/html/rfc2119). Tradução livre [RFC 2119 pt-br](http://rfc.pt.webiwg.org/rfc2119).

## Requisitos
  - Docker
  - Docker compose

## Primeiros passos

Primeiramente é necessário criar a network utilizada para comunicação entre os serviços, para isso execute:

```
docker network create dev-network
```

Posteriormente suba o serviço desejado:

```
docker-compose up -d mysql rabbitmq
```

> Você pode subir quantos serviços desejar ou omitir o nome do serviço para subir todos.

Para que seus outros containeres fora da rede Docker, ou seja, fora deste docker-compose consigam se comunicar com os serviços iniciados no step anterior, será necessário inclui-los dentro da rede, para isso execute:

```
docker network connect dev-network nome-do-container
```

## Licença

Este repo segue os principios de liberdade do software livre. Para saber mais sobre a licença de uso, leia: [MIT license](https://opensource.org/licenses/MIT).
